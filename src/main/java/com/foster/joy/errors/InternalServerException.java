package com.foster.joy.errors;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.foster.joy.helpers.ExceptionsHelper;

public class InternalServerException extends WebApplicationException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InternalServerException(String message) {
         super(new Exception("{\"errorMessage\":\"" + message+ "\"}"),Response.status(Response.Status.INTERNAL_SERVER_ERROR)
             .entity("{\"errorMessage\":\"" + message+ "\"}").type(MediaType.APPLICATION_JSON).build());
     }
	 
	 public InternalServerException(String message,Exception e) {
         super(ExceptionsHelper.getThrowable(e),Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                 .entity("{\"errorMessage\":\"" + message + " " + ExceptionsHelper.getMessageFromException(e)+ "\"}").type(MediaType.APPLICATION_JSON).build());
     }
	 
	 
	 public InternalServerException(Exception e) {
		  super(ExceptionsHelper.getThrowable(e),Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                 .entity("{\"errorMessage\":\"" + ExceptionsHelper.getMessageFromException(e)+ "\"}").type(MediaType.APPLICATION_JSON).build());
     }
	 public InternalServerException(Throwable e) {
		  super(e,Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("{\"errorMessage\":\"" + e.getMessage()+ "\"}").type(MediaType.APPLICATION_JSON).build());
    }
	
}
