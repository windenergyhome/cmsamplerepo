package com.foster.joy.errors;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.foster.joy.helpers.ExceptionsHelper;

public class ForbiddenRequestException extends WebApplicationException {
	 
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ForbiddenRequestException(Exception e) {
        super(ExceptionsHelper.getThrowable(e),Response.status(Response.Status.FORBIDDEN)
                .entity("{\"errorMessage\":\"" + ExceptionsHelper.getMessageFromException(e) + "\"}").type(MediaType.APPLICATION_JSON).build());
    }
	 public ForbiddenRequestException(String message,Exception e) {
        super(ExceptionsHelper.getThrowable(e),Response.status(Response.Status.FORBIDDEN)
                .entity("{\"errorMessage\":\"" + message + " " + ExceptionsHelper.getMessageFromException(e) + "\"}").type(MediaType.APPLICATION_JSON).build());
    }

	 public ForbiddenRequestException(String message) {
        super(new Exception("{\"errorMessage\":\"" + message + "\"}"),Response.status(Response.Status.FORBIDDEN)
                .entity("{\"errorMessage\":\"" + message + "\"}").type(MediaType.APPLICATION_JSON).build());
    }
}
