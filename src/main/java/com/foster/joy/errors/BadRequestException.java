package com.foster.joy.errors;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.foster.joy.helpers.ExceptionsHelper;

public class BadRequestException extends WebApplicationException {
	 
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BadRequestException(Exception e) {
        super(ExceptionsHelper.getThrowable(e),Response.status(Response.Status.BAD_REQUEST)
                .entity("{\"errorMessage\":\"" + ExceptionsHelper.getMessageFromException(e) + "\"}").type(MediaType.APPLICATION_JSON).build());
    }
	 public BadRequestException(String message,Exception e) {
        super(ExceptionsHelper.getThrowable(e),Response.status(Response.Status.BAD_REQUEST)
                .entity("{\"errorMessage\":\"" + message + " " + ExceptionsHelper.getMessageFromException(e) + "\"}").type(MediaType.APPLICATION_JSON).build());
    }

	 public BadRequestException(String message) {
		 super(new Exception("{\"errorMessage\":\"" + message + "\"}"),Response.status(Response.Status.BAD_REQUEST)
                .entity("{\"errorMessage\":\"" + message + "\"}").type(MediaType.APPLICATION_JSON).build());
    }
}
