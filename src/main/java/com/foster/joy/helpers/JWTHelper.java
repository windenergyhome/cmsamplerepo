package com.foster.joy.helpers;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Random;

import com.foster.joy.errors.InternalServerException;
import com.foster.joy.resources.auth.model.ExtractedTokenInfo;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTHelper {

	public static byte[] jwtKey = null;
	public static String JWT_KEY_STRING = "MIIEpQIBAAKCAQEAtfndHr5URt1+CoBm8Jom9jmM/QeGXNv3VdqAMyQjcHGANccT\n"
			+ "GWw5BMRVBAivYQI3ekPAbwyVhHLJPFdXuNxDkAg75ME+0N+qpgXWW/9p3ziClb2g\n"
			+ "HIPltDZ2PwJ77HHS12a5p0OSLN5shjtJic8hPfZMXeoBVPRZYPCGfWfK2YZTZouA\n"
			+ "jHYw/KQU8EszLt6JRDQUp4p0pQ51ukbvj4bSAPaL/I12pXHClEcstg/LPGtemMkr\n"
			+ "5e8+dQ2t7C6W1TY5f8a5ZCAlD5rEbGfgjd1+NjXxoz0dYEPYSFzaDnFzazfaKW/S\n"
			+ "aFjmHrk8yfFRtPBcTFyq+0ETSXp2BWfX4qBNPQIDAQABAoIBACeEk1hRzMt23iA5\n"
			+ "siEd2Mgvx0ZLlAomWpfyU1THzEpm24jSKnui3N8EI9I0cKGIcbPDcs6q1O1mX/0S\n"
			+ "mqIycxmcMCWFMbb6W3yiV+KynLM0+ma6P0LyAbgY+6TDTVYTgc31KOwB6ISF6Ntd\n"
			+ "lKKwsG9cRUk/v/pK9f3vPcWN/Kcv/r3FHUSZ2Z8FeLHHF7mcE2+R2SdEuzHMI0VE\n"
			+ "mzHkpzraPz8hnYHSWojnXABKY0vA4ZrcZO/cvZvSlFzPp9CqoRtMAZpG8+TwYca5\n"
			+ "fYjyGewKk2tXw1hbW5xx/NwJLYsUzBGtZu8sFjFnBNvngek9TfF3zyBBrYPhiW8L\n"
			+ "QS4tsqECgYEA2P7aZvYlROssp1W1zilPgIfZT0/vPtUJcpbQZE31IGgLb97WAjb2\n"
			+ "VBuY4kO6nP018nMnqHbnIZ8trTpXgwZG7EaQ4+Upn4F28Jcd2j4y2eMYPKSn6MEN\n"
			+ "kOfSeAMr4r278GqF19HQsTcd9P1N7tMzRzei7DYroWom6FOwtJM1tpcCgYEA1q+S\n"
			+ "rqNIdsXvTYcEULgSUzX/9M8544cS4ScQE/04Be6oKHd+0XlG+SIphpqClsCk/kh9\n"
			+ "r4jnWUOGNIKRfW0Jv1/T5KLtofpG0Yv8GPl586XHO2986O2EfVfzHpcI9FMScukx\n"
			+ "xkUHm0N3MUxYBXoAAtPd2oqlPffOfklNgrDbiUsCgYEAx+aPoIZlcwYLjuQApjmc\n"
			+ "SdXcNZp31jQ/jdath5nhlccDsj1gfY3Qg08hGmA2i+UgS4gWIrXW2zxewdSbRdME\n"
			+ "hynC1I8KNyz3O1CbabAe/Gaer08od91Q+Ar38yNIzIrpleMsBdP+IyOZb1Xi836R\n"
			+ "IKx6ZIzKyNqXmTNv+AodU2UCgYEAljOokH61CERbtQYtwrvGNmmldoY1nja2wsgt\n"
			+ "u46CT8Q16noQREZxnYSFQSOBEjAeP7pAFTvXgOB5WOuxFuve0gIdoe1MDPOp8Jnb\n"
			+ "+y0wYYMu7SqHXgAvnfKWqmRJY4drdPIqXOmW9M4y6k7blplQwKmyomA2VDC4TCrF\n"
			+ "DBbowhkCgYEAlqrzx59mGSV8TvZp9t4zTJAdYGz0BLA117cab2hyYNkTQ+tckYM2\n"
			+ "6xcWM7pr8cHR37DywkSs+AcNKLoZGdfoRy+VWMZXaUz5ovp/4n/cQWodDG/pYpB3\n"
			+ "9/3r1g8rAGcVm58gjUBMLd/XOlG6P+TmwZdVU+EMBOujUdxig5G0aec=";

	public static byte[] getJwtKey() {
		if (JWTHelper.jwtKey != null) {
			return JWTHelper.jwtKey;
		} else {
			byte[] bts;
			bts = JWT_KEY_STRING.getBytes();
			JWTHelper.jwtKey = bts;
			return JWTHelper.jwtKey;

		}
	}

	private static StringBuilder getStringFromBytes(byte[] enc) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < enc.length; i++) {
			sb.append(Integer.toString((enc[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb;
	}

	private static String getMD5Hash(String val) throws UnsupportedEncodingException {
		String generatedPassword = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] currentBytes = md.digest(val.getBytes());

			StringBuilder sb = getStringFromBytes(currentBytes);
			generatedPassword = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return generatedPassword;
	}

	private static int REFRESH_LENGTH = 32;

	public static String getNewRefreshToken() {
		StringBuilder ret = new StringBuilder();
		Random rand = new Random();
		for (int i = 0; i < REFRESH_LENGTH; i++) {

			int val = rand.nextInt();
			String hexVal = Integer.toHexString(val);
			ret.append(hexVal);
		}
		return ret.toString();
	}

	public static String getNewJWT(Long issueTime, Long validForTimeInMs, String userId) {
		try {
			Date issDate = null;
			byte[] apiKeySecretBytes = JWTHelper.getJwtKey();
			// The JWT signature algorithm we will be using to sign the token
			SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
	
			if (issueTime == null) {
				issDate = new Date();

				issueTime=issDate.getTime();
			}else {
				issDate = new Date(issueTime);
			}
	
			// Let's set the JWT Claims
			JwtBuilder builder = Jwts.builder().setIssuedAt(issDate).setSubject(userId);
	
			// if it has been specified, let's add the expiration
			if (validForTimeInMs == null || validForTimeInMs < 0) {
				validForTimeInMs = 10 * 60 * 1000L;
			}
			long expMillis = issueTime + validForTimeInMs;
			Date exp = new Date(expMillis);
			builder.setExpiration(exp);
			builder.setHeaderParam("typ", "JWT");
			builder.signWith(signatureAlgorithm, apiKeySecretBytes);
			// builder.signWith(signatureAlgorithm, signingKey);
			// Builds the JWT and serializes it to a compact, URL-safe string
			String ret = builder.compact();
	
			return ret;
		}catch(Exception e) {
			throw new InternalServerException(e);
		}
	}

	public static ExtractedTokenInfo extractToken(String token) throws Exception {

		byte[] apiKeySecretBytes = JWTHelper.getJwtKey();
		// The JWT signature algorithm we will be using to sign the token
		JwtParser parser = Jwts.parser();
		parser.setSigningKey(apiKeySecretBytes);
		Jws<Claims> jws = null;
		try {
			jws = parser.parseClaimsJws(token);
		}catch(ExpiredJwtException e){
			ExtractedTokenInfo et = new ExtractedTokenInfo();
			et.setExpired(true);
			et.setExpiresAt(e.getClaims().getExpiration().getTime());
			et.setIssuedAt(e.getClaims().getIssuedAt().getTime());
			et.setUserEmail(e.getClaims().getSubject());
			return et;
		} catch (Exception e) {
			System.out.println("Problem extracting token or token is expired: " + e.getMessage());
			throw e;
		}
		ExtractedTokenInfo et = new ExtractedTokenInfo();
		if (jws == null) {
			throw new Exception("Could not extract JWT Token");
		} else {
			if (jws.getBody() != null) {
				Date expirey = jws.getBody().getExpiration();
				if (expirey != null) {
					et.setExpiresAt(expirey.getTime());
				} else {
					// no expiration
					et.setExpiresAt(null);
				}
				et.setUserEmail(jws.getBody().getSubject());


			}
		}
		return et;
	}
}
