package com.foster.joy.helpers;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class GravatarHelper {
	private static String GRAVATAR_URL ="https://www.gravatar.com/avatar/";
	//getAvatar method returns the actual image in case you want it instead of the url
	/*
	public static String getAvatar(String email) throws IOException {
		String url = getGravatarUrl(email);
		String response = HTTPHelper.makeHttpCall(url, null, "GET");
		return response;
	}
	*/

	public static String getGravatarUrl(String email) {
		String hash = GravatarHelper.md5Hex(email);
		String url=GravatarHelper.GRAVATAR_URL + hash;
		return url;
	}
	
	public static String hex(byte[] array) {
	      StringBuffer sb = new StringBuffer();
	      for (int i = 0; i < array.length; ++i) {
	      sb.append(Integer.toHexString((array[i]
	          & 0xFF) | 0x100).substring(1,3));        
	      }
	      return sb.toString();
	  }
	  public static String md5Hex (String message) {
	      try {
	      MessageDigest md = 
	          MessageDigest.getInstance("MD5");
	      return hex (md.digest(message.getBytes("CP1252")));
	      } catch (NoSuchAlgorithmException e) {
	      } catch (UnsupportedEncodingException e) {
	      }
	      return null;
	  }
}
