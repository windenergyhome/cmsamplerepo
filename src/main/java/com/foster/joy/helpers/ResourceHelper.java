package com.foster.joy.helpers;

import com.foster.joy.errors.BadRequestException;
import com.foster.joy.errors.ForbiddenRequestException;
import com.foster.joy.resources.auth.dao.AuthCacheImpl;
import com.foster.joy.resources.auth.dao.AuthDAO;
import com.foster.joy.resources.auth.model.ExtractedTokenInfo;
import com.foster.joy.resources.ideas.dao.IdeasCacheImpl;
import com.foster.joy.resources.ideas.dao.IdeasDAO;
import com.foster.joy.resources.users.dao.UsersCacheImpl;
import com.foster.joy.resources.users.dao.UsersDAO;

public class ResourceHelper {
	private static AuthDAO  authDao = new AuthCacheImpl();
	private static IdeasDAO ideasDao = new IdeasCacheImpl();
	private static UsersDAO usersDao = new UsersCacheImpl();

	public static AuthDAO getAuthDAO() {
		return authDao;
	}

	public static IdeasDAO getIdeasDAO() {
		return ideasDao;
	}

	public static UsersDAO getUsersDAO() {
		return usersDao;
	}
	public static String getEmailFromToken(String token) {
		String email = null;
		ResourceHelper.CheckForMissingToken(token);
		ExtractedTokenInfo ti;
		try {
			ti = JWTHelper.extractToken(token);
			ResourceHelper.CheckForExpiredToken(ti);
			email = ti.getUserEmail();
		} catch (Exception e) {
			throw new BadRequestException("Token is invalid.");
		}
		return email;
	}

	private static void CheckForExpiredToken(ExtractedTokenInfo ti) {
		if (ti.isExpired()) {
			throw new ForbiddenRequestException("Token is expired.");
		}
	}

	private static void CheckForMissingToken(String token) {
		if (token == null) {
			throw new BadRequestException("Token is required.");
		}
	}
}
