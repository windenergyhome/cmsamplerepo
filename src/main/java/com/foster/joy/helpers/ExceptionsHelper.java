package com.foster.joy.helpers;

import javax.ws.rs.WebApplicationException;

public class ExceptionsHelper {
	 public static Throwable getThrowable(Exception e){
		 Throwable ret=null;
		 if (e instanceof WebApplicationException){
			 ret = e.getCause();
		 }else{
			 ret = e;
		 }
		 return ret;
        
	 }
	 public static String getMessageFromException(Exception e){
		 String eMessage = "";
		 if (e instanceof WebApplicationException){
			 eMessage = e.getCause().getMessage();
		 }else{
			 eMessage = e.getMessage();
		 }
		 return eMessage;
        
	 }
}
