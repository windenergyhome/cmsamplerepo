//This was used to get the gravatar image but realized we just need the url, so no longer needed

//package com.foster.joy.helpers;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.io.OutputStreamWriter;
//import java.io.UnsupportedEncodingException;
//import java.net.HttpURLConnection;
//import java.net.URL;
//
//
//public class HTTPHelper {
//	public static void handleNonSuccessResponse(HttpURLConnection connection,
//			String message) throws IOException
//	{
//		Integer responseCode = null;
//		String responseMessage = "";
//		String additionalErrorMessage = "";
//		String fullErrorMessage = "";
//		try {
//			if (connection != null) {
//				responseCode = connection.getResponseCode();
//				responseMessage = connection.getResponseMessage();
//			}
//			if (message != null) {
//				additionalErrorMessage = message;
//			}
//			if (responseCode != null) {
//				fullErrorMessage = fullErrorMessage + " Response Code: " + responseCode;
//			}
//			if (responseMessage != null && !"".equals(responseMessage)) {
//				fullErrorMessage = fullErrorMessage + " Response Message: " + responseMessage;
//			}
//			if (additionalErrorMessage != null && !"".equals(additionalErrorMessage)) {
//				fullErrorMessage = fullErrorMessage + " Error Message: " + additionalErrorMessage;
//			}
//			System.out.println("Non-success response: " + fullErrorMessage);
//			throw new IOException("Non-success response: " + fullErrorMessage);
//		} catch (IOException ex) {
//			if (responseCode == null) {
//				System.out.println("Non-success response: " + responseMessage);
//			} else {
//				System.out.println("Non-success response: " + responseCode + " -- " + responseMessage);
//			}
//			throw ex;
//		}
//	}
//	public static HttpURLConnection getConnection(String urlString,
//			String body, String method)
//			throws IOException {
//		URL url = new URL(urlString);
//		if (method != null){
//			method=method.toUpperCase();
//		}
//		
//		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//		if (method != null) {
//			connection.setRequestMethod(method);
//		}
//		connection.setReadTimeout(1000 * 30);
//		connection.setConnectTimeout(1000 * 10);
//		connection.setInstanceFollowRedirects(true);
//		
//		if (body != null && !"GET".equals(method) && !"HEAD".equals(method)) {
//			connection.setDoOutput(true);
//			OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
//			wr.write(body);
//			wr.flush();
//			wr.close();
//		} 
//		return connection;
//	}
//	
//	
//
//
//	public static String makeHttpCall(
//			String urlWithQueryString, String body,
//			String method)
//			throws IOException {
//		HttpURLConnection connection = null;
//		InputStream inputStream = null;
//		String newUrl=null;
//		int redirectCount=0;
//		try {
//			connection = HTTPHelper.getConnection(urlWithQueryString, body, method);
//			
//			int responseCode = connection.getResponseCode();
//
//			boolean redirect = false;
//
//			// normally, 3xx is redirect
//			if (responseCode != HttpURLConnection.HTTP_OK) {
//				if (responseCode == HttpURLConnection.HTTP_MOVED_TEMP
//					|| responseCode == HttpURLConnection.HTTP_MOVED_PERM
//						|| responseCode == HttpURLConnection.HTTP_SEE_OTHER)
//				redirect = true;
//			}
//
//			redirectCount = 0;
//
//			while (redirect && redirectCount < 10) {
//				redirectCount++;
//				// get redirect url from "location" header field
//				newUrl = connection.getHeaderField("Location");
//
//				// get the cookie if need, for login
//				String cookies = connection.getHeaderField("Set-Cookie");
//
//				connection.disconnect();
//				// open the new connnection again
//				connection = HTTPHelper.getConnection(newUrl, body, method);
//				
//				if (cookies != null && !cookies.equals("")){
//					//if (!urlWithQueryString.contains("nytimes")){
//						//String a="b";
//					//}
//					 connection.setRequestProperty("Cookie", cookies);
//				}
//				System.out.println("Redirect from URL: " +  urlWithQueryString +  " to URL : " + newUrl + " Response Code ... " + responseCode);
//				responseCode = connection.getResponseCode();
//				redirect=false;
//				if (responseCode != HttpURLConnection.HTTP_OK) {
//					if (responseCode == HttpURLConnection.HTTP_MOVED_TEMP
//						|| responseCode == HttpURLConnection.HTTP_MOVED_PERM
//							|| responseCode == HttpURLConnection.HTTP_SEE_OTHER)
//					redirect = true;
//				}
//			}
//			if (redirectCount>1){
//				System.out.println("redirected " + redirectCount + " times");
//			}
//			inputStream = connection.getErrorStream();
//			if (inputStream == null) {
//				if (!"HEAD".equals(method)) {
//					inputStream = connection.getInputStream();
//				} else {
//					if (responseCode >= 200 && responseCode <= 299) {
//						return "";
//					}
//				}
//
//			}
//			String fullResponse = getResponseTextFromInputStream(inputStream);
//			if (responseCode >= 200 && responseCode <= 299) {
//				return fullResponse;
//			} else {
//				HTTPHelper.handleNonSuccessResponse(connection, fullResponse);
//			}
//
//		} catch (Exception e) {
//			if (connection != null) {
//				HTTPHelper.handleNonSuccessResponse(connection, e.getMessage());
//			}
//		} finally {
//			if (inputStream != null) {
//				try {
//
//					inputStream.close();
//				} catch (IOException e) {
//					System.out.println(e.getMessage());
//					// do nothing
//				}
//			}
//			if (connection != null) {
//				try {
//
//					connection.disconnect();
//				} catch (Exception e) {
//					System.out.println(e.getMessage());
//					// do nothing
//				}
//			}
//
//		}
//		return null;
//
//	}
//
//	public static String getResponseTextFromInputStream(
//			InputStream inputStream) throws UnsupportedEncodingException,
//			IOException {
//		BufferedReader reader = new BufferedReader(new InputStreamReader((inputStream), "UTF-8"));
//		StringBuilder fullResponse = new StringBuilder();
//		try {
//			String line = reader.readLine();
//
//			while (line != null) {
//				// System.out.println(line);
//				fullResponse.append(line);
//				line = reader.readLine();
//
//			}
//			reader.close();
//		} catch (IOException e) {
//			HTTPHelper.handleNonSuccessResponse(null, e.getMessage());
//		}
//		return fullResponse.toString();
//	}
//
//
//}
