package com.foster.joy.resources.ideas.dao;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.foster.joy.errors.BadRequestException;
import com.foster.joy.resources.ideas.model.Idea;
import com.foster.joy.resources.ideas.model.SimpleIdea;

public class IdeasCacheImpl implements IdeasDAO {
	private Map<String,List<Idea>> ideaMap = new HashMap<String,List<Idea>>();
	private Map<String, Boolean> needsSortingMap = new HashMap<String, Boolean>();
	private Long nextIdeaId = 1000L;
	
	private String getNextId() {
		nextIdeaId++;
		return nextIdeaId.toString();
	}
	@Override
	public Idea createIdea(String userEmail, SimpleIdea idea) {
		String id = getNextId();
		Instant in= Instant.now();
		Idea newIdea = new Idea(idea);
		newIdea.setCreated_at(in.toEpochMilli());
		newIdea.setId(id);
		List<Idea> userIdeas = ideaMap.get(userEmail);
		if (userIdeas==null) {
			userIdeas = new ArrayList<Idea>();
			ideaMap.put(userEmail, userIdeas);
		}
		userIdeas.add(newIdea);
		needsSortingMap.put(userEmail, true); //flag that sort is needed
		return newIdea;
	}

	@Override
	public Idea updateIdea(String userEmail, String ideaId, SimpleIdea idea) throws BadRequestException{
		List<Idea> userIdeas = ideaMap.get(userEmail);
		Idea oldIdea = getExistingIdeaFailIfNotFound(ideaId, userIdeas);
		oldIdea.setConfidence(idea.getConfidence());
		oldIdea.setContent(idea.getContent());
		oldIdea.setEase(idea.getEase());
		oldIdea.setImpact(idea.getImpact());
		needsSortingMap.put(userEmail, true); //flag that sort is needed as some fields used for sort could have been changed
		// TODO Auto-generated method stub
		return oldIdea;
	}

	@Override
	public List<Idea> getIdeas(String userEmail, Integer page, Integer size) {
		Boolean needsSort = needsSortingMap.get(userEmail);
		if (needsSort==null) {
			needsSort = false;
		}
		List<Idea> userIdeas = ideaMap.get(userEmail);
		if (userIdeas != null) {
			if (needsSort) {
				Collections.sort(userIdeas, Collections.reverseOrder());	
			}
			int start = (page-1)*size;
			int end = start + size;
			if (start > userIdeas.size()) {
				return null;
			}
			if (end > userIdeas.size()) {
				end = userIdeas.size();
			}
			return userIdeas.subList(start,end);
		}else {
			return null;
		}
	}

	@Override
	public void deleteIdea(String userEmail, String ideaId) throws BadRequestException {
		List<Idea> userIdeas = ideaMap.get(userEmail);
		Idea oldIdea = getExistingIdeaFailIfNotFound(ideaId, userIdeas);
		userIdeas.remove(oldIdea);
	}
	
	private Idea getExistingIdeaFailIfNotFound(String ideaId, List<Idea> userIdeas) {
		if (userIdeas==null) {
			throw new BadRequestException("Idea can not be found");
		}
		Idea oldIdea = null;
		for (Idea id : userIdeas) {
			if (id.getId() != null && id.getId().equals(ideaId)) {
				oldIdea = id;
				break;
			}
		}
		if (oldIdea==null) {
			throw new BadRequestException("Idea can not be found");
		}
		return oldIdea;
	}

}
