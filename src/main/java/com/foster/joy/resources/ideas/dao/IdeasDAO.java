package com.foster.joy.resources.ideas.dao;

import java.util.List;

import com.foster.joy.errors.BadRequestException;
import com.foster.joy.resources.ideas.model.Idea;
import com.foster.joy.resources.ideas.model.SimpleIdea;

public interface IdeasDAO {
	public Idea createIdea(String userEmail, SimpleIdea idea);
	public Idea updateIdea(String userEmail, String ideaId, SimpleIdea idea) throws BadRequestException;
	public List<Idea> getIdeas(String userEmail, Integer page, Integer size);
	public void deleteIdea(String userEmail, String ideaId) throws BadRequestException;
}
