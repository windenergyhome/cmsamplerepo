package com.foster.joy.resources.ideas.endpoint;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.foster.joy.errors.BadRequestException;
import com.foster.joy.errors.ForbiddenRequestException;
import com.foster.joy.errors.InternalServerException;
import com.foster.joy.helpers.ResourceHelper;
import com.foster.joy.resources.auth.endpoint.AuthResource;
import com.foster.joy.resources.ideas.dao.IdeasDAO;
import com.foster.joy.resources.ideas.model.Idea;
import com.foster.joy.resources.ideas.model.SimpleIdea;

@Path("/ideas")
public class IdeasResource {
	private IdeasDAO dao = ResourceHelper.getIdeasDAO();

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response addIdea(@HeaderParam("X-Access-Token") String token, SimpleIdea idea) {
		try {
			AuthResource.checkAccessToken(token);
			checkIdea(idea);
			String email = ResourceHelper.getEmailFromToken(token);
			Idea newIdea = dao.createIdea(email, idea);
			return Response.status(Response.Status.CREATED).entity(newIdea).build();
		} catch (BadRequestException | ForbiddenRequestException bre) {
			throw bre;
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}

	@DELETE
	@Path("/{ideaId}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response deleteIdea(@HeaderParam("X-Access-Token") String token, @PathParam("ideaId") String ideaId) {
		try {
			AuthResource.checkAccessToken(token);
			String email = ResourceHelper.getEmailFromToken(token);
			dao.deleteIdea(email, ideaId);
			return Response.status(Response.Status.NO_CONTENT).build();
		} catch (BadRequestException | ForbiddenRequestException bre) {
			throw bre;
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}

	@PUT
	@Path("/{ideaId}")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Idea updateIdea(@HeaderParam("X-Access-Token") String token, @PathParam("ideaId") String ideaId,
			SimpleIdea idea) {
		try {
			AuthResource.checkAccessToken(token);
			checkIdea(idea);
			String email = ResourceHelper.getEmailFromToken(token);
			return dao.updateIdea(email, ideaId, idea);
		} catch (BadRequestException | ForbiddenRequestException bre) {
			throw bre;
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Idea> getPageOfIdeas(@HeaderParam("X-Access-Token") String token, @QueryParam("page") Integer page) {
		try {
			AuthResource.checkAccessToken(token);
			if (page == null) {
				page = 1;
			}
			String email = ResourceHelper.getEmailFromToken(token);
			return dao.getIdeas(email, page, 10);
		} catch (BadRequestException | ForbiddenRequestException bre) {
			throw bre;
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}

	private void checkIdea(SimpleIdea idea) {
		if (idea == null) {
			throw new BadRequestException("Idea details are required");
		}
		if (idea.getConfidence() == null || idea.getConfidence() < 0 || idea.getConfidence() > 10) {
			throw new BadRequestException("Idea confidence between 0 and 10 must be specified");
		}
		if (idea.getEase() == null || idea.getEase() < 0 || idea.getEase() > 10) {
			throw new BadRequestException("Idea ease between 0 and 10 must be specified");
		}
		if (idea.getImpact() == null || idea.getImpact() < 0 || idea.getImpact() > 10) {
			throw new BadRequestException("Idea impact between 0 and 10 must be specified");
		}
		if (idea.getContent() == null || idea.getContent().length() > 255) {
			throw new BadRequestException("Idea content of up to 255 characters must be specified");
		}
	}
}
