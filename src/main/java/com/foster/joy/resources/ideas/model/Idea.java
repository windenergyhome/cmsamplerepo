package com.foster.joy.resources.ideas.model;

public class Idea extends SimpleIdea implements Comparable<Idea>{
	private String id = null;
	private Long created_at = null;

	public Idea(SimpleIdea i) {
		this.setConfidence(i.getConfidence());
		this.setContent(i.getContent());
		this.setEase(i.getEase());
		this.setImpact(i.getImpact());
	}

	public Double getAverage_score() {
		Double tot = 0D;

		if (this.getConfidence() != null) {
			tot += this.getConfidence();
		}
		if (this.getEase() != null) {
			tot += this.getEase();
		}
		if (this.getImpact() != null) {
			tot += this.getImpact();
		}
		return tot / 3;
	}

	/** epoch in seconds aparently **/
	public Long getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Long created_at) {
		this.created_at = created_at;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public int compareTo(Idea o) {
		if (o==null) {
			return 1;
		}
		return Double.compare(this.getAverage_score(), o.getAverage_score());
	}
}
