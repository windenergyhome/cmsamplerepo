package com.foster.joy.resources.ideas.model;

public class SimpleIdea {

	private String content = null;
	private Integer impact = null;
	private Integer ease = null;
	private Integer confidence = null;
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Integer getImpact() {
		return impact;
	}
	public void setImpact(Integer impact) {
		this.impact = impact;
	}
	public Integer getEase() {
		return ease;
	}
	public void setEase(Integer ease) {
		this.ease = ease;
	}
	public Integer getConfidence() {
		return confidence;
	}
	public void setConfidence(Integer confidence) {
		this.confidence = confidence;
	}
	
}
