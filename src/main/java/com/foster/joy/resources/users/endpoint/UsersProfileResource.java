package com.foster.joy.resources.users.endpoint;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.foster.joy.errors.BadRequestException;
import com.foster.joy.errors.ForbiddenRequestException;
import com.foster.joy.errors.InternalServerException;
import com.foster.joy.helpers.ResourceHelper;
import com.foster.joy.resources.auth.endpoint.AuthResource;
import com.foster.joy.resources.users.dao.UsersDAO;
import com.foster.joy.resources.users.model.User;

@Path("/me")
public class UsersProfileResource {
	private UsersDAO dao = ResourceHelper.getUsersDAO();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public User signup(@HeaderParam("X-Access-Token") String token) {
		try {
			AuthResource.checkAccessToken(token);
			String email = ResourceHelper.getEmailFromToken(token);
			return dao.getUserProfile(email);
		} catch (BadRequestException | ForbiddenRequestException bre) {
			throw bre;
		} catch (Exception e) {
			throw new InternalServerException(e);
		}

	}

}
