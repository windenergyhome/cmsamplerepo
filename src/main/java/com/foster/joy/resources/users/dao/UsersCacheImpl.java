package com.foster.joy.resources.users.dao;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.foster.joy.errors.BadRequestException;
import com.foster.joy.errors.InternalServerException;
import com.foster.joy.helpers.GravatarHelper;
import com.foster.joy.resources.users.model.User;
import com.foster.joy.resources.users.model.UserWithPassword;

public class UsersCacheImpl implements UsersDAO{
	//in a production environment would need to to store password encrypted and compare hash values
	Map<String,UserWithPassword> usersByEmail = new HashMap<String,UserWithPassword>();

	@Override
	public UserWithPassword getUserByEmail(String email) {
		return usersByEmail.get(email);
	}


	@Override
	public void addUser(UserWithPassword newUser) {
		if (newUser.getEmail()==null || "".equals(newUser.getEmail())) {
			throw new BadRequestException("Missing Email");
		}
		if (newUser.getName()==null || "".equals(newUser.getName())) {
			throw new BadRequestException("Missing Name");
		}
		if (newUser.getPassword()==null || "".equals(newUser.getPassword())) {
			throw new BadRequestException("Missing Password");
		}
		if (newUser.getPassword().length()<8) {
			//this could be in the regex, but want to give it a better message
			throw new BadRequestException("Password is too short.");
		}
		 if( !newUser.getPassword().matches("(?=.*[0-9]).*") ) {
			 throw new BadRequestException("Password must have at least 1 number.");
		 }
		 if( !newUser.getPassword().matches("(?=.*[a-z]).*") ) {
			 throw new BadRequestException("Password must have at least 1 lower case letter.");
		 }
		 if( !newUser.getPassword().matches("(?=.*[A-Z]).*") ) {
			 throw new BadRequestException("Password must have at least 1 upper case letter.");
		 }
	                    
		 if( !newUser.getEmail().toUpperCase().matches("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$") ) {
			 throw new BadRequestException("Invalid Email");
		 }
		UserWithPassword u = usersByEmail.get(newUser.getEmail());
		if (u!=null) {
			throw new BadRequestException("User with this email already exists");
		}
		usersByEmail.put(newUser.getEmail(), newUser);
	}


	@Override
	public User getUserProfile(String email) {
		UserWithPassword u = usersByEmail.get(email);
		if (u==null) {
			throw new BadRequestException("Can't find User");
		}
		String avUrl = GravatarHelper.getGravatarUrl(email);
		User ret = new User(u,avUrl);
		return ret;
		
	}

}
