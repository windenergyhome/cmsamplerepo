package com.foster.joy.resources.users.model;

public class SimpleUser {
	private String email = null;
	private String name = null;
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
}
