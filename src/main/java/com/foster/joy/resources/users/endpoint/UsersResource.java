package com.foster.joy.resources.users.endpoint;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.foster.joy.errors.BadRequestException;
import com.foster.joy.errors.ForbiddenRequestException;
import com.foster.joy.errors.InternalServerException;
import com.foster.joy.helpers.ResourceHelper;
import com.foster.joy.resources.auth.dao.AuthDAO;
import com.foster.joy.resources.auth.model.AccessTokenWithRefreshToken;
import com.foster.joy.resources.auth.model.LoginRequestParams;
import com.foster.joy.resources.users.dao.UsersDAO;
import com.foster.joy.resources.users.model.UserWithPassword;

@Path("/users")
public class UsersResource {
	private UsersDAO uDao = ResourceHelper.getUsersDAO();
	private AuthDAO aDao = ResourceHelper.getAuthDAO();

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces("application/json")
	public Response signup(UserWithPassword userInfo) {
		try {
			if (userInfo == null) {
				throw new BadRequestException("User info is required");
			}
			uDao.addUser(userInfo);
			LoginRequestParams params = new LoginRequestParams();
			params.setEmail(userInfo.getEmail());
			params.setPassword(userInfo.getPassword());
			AccessTokenWithRefreshToken tok = aDao.getNewAccessToken(params);
			return Response.status(Response.Status.CREATED).entity(tok).build();
		} catch (BadRequestException | ForbiddenRequestException bre) {
			throw bre;
		} catch (Exception e) {
			throw new InternalServerException(e);
		}

	}

}
