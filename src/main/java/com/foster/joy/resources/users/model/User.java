package com.foster.joy.resources.users.model;

public class User extends SimpleUser{
	private String avatar_url=null;

	public User(SimpleUser u, String aurl) {
		this.setEmail(u.getEmail());
		this.setName(u.getName());
		this.setAvatar_url(aurl);
	}
	public String getAvatar_url() {
		return avatar_url;
	}

	public void setAvatar_url(String avatar_url) {
		this.avatar_url = avatar_url;
	}
	
}
