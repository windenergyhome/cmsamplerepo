package com.foster.joy.resources.users.dao;

import com.foster.joy.resources.users.model.User;
import com.foster.joy.resources.users.model.UserWithPassword;

public interface UsersDAO {
	public UserWithPassword getUserByEmail(String email);
	public void addUser(UserWithPassword newUser);
	public User getUserProfile(String email);
}
