package com.foster.joy.resources.users.model;

public class UserWithPassword extends SimpleUser{
	private String password = null;

	public String getPassword() {
			return password;
		}
	
		public void setPassword(String password) {
			this.password = password;
		}
	
}
