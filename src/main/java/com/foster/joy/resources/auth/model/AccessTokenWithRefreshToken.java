package com.foster.joy.resources.auth.model;

public class AccessTokenWithRefreshToken extends AccessToken{
	private String refresh_token = null;

	public String getRefresh_token() {
		return refresh_token;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}
}
