package com.foster.joy.resources.auth.model;

public class ExtractedTokenInfo {
	private String userEmail = null;
	private Long issuedAt = null;
	private Long expiresAt = null;
	private boolean isExpired = false;
	
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public Long getIssuedAt() {
		return issuedAt;
	}
	public void setIssuedAt(Long issuedAt) {
		this.issuedAt = issuedAt;
	}
	public Long getExpiresAt() {
		return expiresAt;
	}
	public void setExpiresAt(Long expiresAt) {
		this.expiresAt = expiresAt;
	}
	public boolean isExpired() {
		return isExpired;
	}
	public void setExpired(boolean isExpired) {
		this.isExpired = isExpired;
	}
	
}
