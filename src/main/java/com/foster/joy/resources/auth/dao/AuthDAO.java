package com.foster.joy.resources.auth.dao;

import com.foster.joy.errors.BadRequestException;
import com.foster.joy.errors.ForbiddenRequestException;
import com.foster.joy.resources.auth.model.AccessToken;
import com.foster.joy.resources.auth.model.AccessTokenWithRefreshToken;
import com.foster.joy.resources.auth.model.LoginRequestParams;

public interface AuthDAO {
	public void deleteRefreshToken(String refreshToken, String userEmail);
	public void deleteAccessToken(String token, String userEmail);
	public AccessTokenWithRefreshToken getNewAccessToken(LoginRequestParams params) throws BadRequestException,ForbiddenRequestException	;
	public boolean isAccessTokenValid(String token,String userEmail) ;
	public AccessToken refreshAccessToken( String refreshToken) ;
}
