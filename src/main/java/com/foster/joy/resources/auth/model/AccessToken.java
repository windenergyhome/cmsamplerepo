package com.foster.joy.resources.auth.model;

public class AccessToken {
	private String jwt=null;

	public String getJwt() {
		return jwt;
	}

	public void setJwt(String jwt) {
		this.jwt = jwt;
	}
}
