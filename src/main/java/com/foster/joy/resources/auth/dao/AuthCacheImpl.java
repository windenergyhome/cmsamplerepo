package com.foster.joy.resources.auth.dao;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import com.foster.joy.errors.BadRequestException;
import com.foster.joy.errors.ForbiddenRequestException;
import com.foster.joy.helpers.JWTHelper;
import com.foster.joy.helpers.ResourceHelper;
import com.foster.joy.resources.auth.model.AccessToken;
import com.foster.joy.resources.auth.model.AccessTokenWithRefreshToken;
import com.foster.joy.resources.auth.model.ExtractedTokenInfo;
import com.foster.joy.resources.auth.model.LoginRequestParams;
import com.foster.joy.resources.users.model.UserWithPassword;
//in a production application, would have a delegate DAOImpl to the actual db and only cache certain items
	//all updates must be delegated to delegate and updated in cache if this is backed by a real db
public class AuthCacheImpl implements AuthDAO{
	private static Long refreshTokenExpiration = 1000*60*60*24*30L; //30 days in ms
	private Map<String,List<String>> userAccessTokens = new HashMap<String,List<String>>();
	private Map<String,String> userRefreshTokens = new HashMap<String,String>();
	private Map<String,Instant> userRefreshTokenExpirationTimes = new HashMap<String,Instant>();
	
	@Override
	public void deleteRefreshToken(String refreshToken,String userEmail) {
		//user only has one refresh token, so if this matches, empty it out
				if (userEmail != null) {
					String savedRefreshToken =userRefreshTokens.get(userEmail);
					if (savedRefreshToken  != null && savedRefreshToken.equals(refreshToken)) {
						userRefreshTokens.put(userEmail, null);
						userRefreshTokenExpirationTimes.put(userEmail, null);
					}				
				}
		
	}

	@Override
	public void deleteAccessToken(String aToken,String userEmail) {
	
		
		if (aToken != null) {
			List<String> tokens = userAccessTokens.get(userEmail);
			tokens.remove(aToken); //if it is in there, remove it
		}
		
		
	}


	@Override
	public AccessTokenWithRefreshToken getNewAccessToken(LoginRequestParams params) throws BadRequestException,ForbiddenRequestException	 {
		String email = params.getEmail();
		if (email==null || "".equals(email)) {
			throw new BadRequestException("Email cannot be empty");
		}
		if (params.getPassword()==null || "".equals(params.getPassword())) {
			throw new BadRequestException("Password cannot be empty");
		}
		UserWithPassword u = ResourceHelper.getUsersDAO().getUserByEmail(params.getEmail());
		if (u==null) {
			//give same error for bad email and bad username to not give away to end user which is wrong
			throw new ForbiddenRequestException("Invalid Email or Password");
		}
		//in prod, if this was getting pw from db, would compare hashes, not actual passwords
		if (params.getPassword().equals(u.getPassword())) {
			String jws = getNewJWS(email);
			String refreshToken = userRefreshTokens.get(email);
			if (refreshToken == null || hasRefreshTokenExpired(email)) {
				refreshToken = JWTHelper.getNewRefreshToken();
				userRefreshTokens.put(email,refreshToken);
				userRefreshTokenExpirationTimes.put(email, getNextRefreshExpiration());
			}
			AccessTokenWithRefreshToken ret = new AccessTokenWithRefreshToken();
			ret.setJwt(jws);
			ret.setRefresh_token(refreshToken);
			return ret;
		}else {
			//give same error for bad email and bad username to not give away to end user which is wrong
			throw new ForbiddenRequestException("Invalid Email or Password");
		}
	}

	
	@Override
	public boolean isAccessTokenValid(String token,String userEmail) {
		boolean ret = false;
		List<String> tokens = userAccessTokens.get(userEmail);
		if (tokens != null) {
			if (tokens.indexOf(token)> -1) {
				try {
					ExtractedTokenInfo eti = JWTHelper.extractToken(token);
					if (!eti.isExpired()) {
						ret=true;
					}
				}catch (Exception e) {
					//is expired
					ret=false;
				}
			}
		}
		return ret;
	}

	@Override
	public AccessToken refreshAccessToken(String refreshToken) {
		if (refreshToken==null) {
			throw new BadRequestException("Refresh Token cannot be empty");
		}
		try {
			
			String userEmail = getRefreshKeyEmail(refreshToken);
			if (userEmail == null ) {
				throw new BadRequestException("Invalid Refresh Token");
			}
			if (this.hasRefreshTokenExpired(userEmail)) {
				throw new BadRequestException("Refresh Token has expired");
			}
			
			String jws = getNewJWS(userEmail);
			AccessToken ret = new AccessToken();
			ret.setJwt(jws);
			return ret;
		} catch (BadRequestException | ForbiddenRequestException  bre) {						
			throw bre;
		}catch (Exception e) {
			//is expired
			throw new BadRequestException(e);
		}
	}
	

	private Instant getNextRefreshExpiration() {
		Instant n = Instant.now();
		n=n.plusMillis(AuthCacheImpl.refreshTokenExpiration);
		return n;
	}
	private boolean hasRefreshTokenExpired(String email) {
		Instant e = userRefreshTokenExpirationTimes.get(email);
		if (e==null) {
			return true;
		}
		if (e.isBefore(Instant.now())) {
			return true;
		}
		return false;
	}
	private String getNewJWS(String email) {
		String jws =  JWTHelper.getNewJWT(null, null, email);
		List<String> keys = userAccessTokens.get(email);
		if (keys==null) {
			keys = new ArrayList<String>();
			userAccessTokens.put(email, keys);
		}
		if (!keys.contains(jws)) {
			keys.add(jws);
		}
		return jws;
	}
	private String getRefreshKeyEmail(String refreshToken) {
		Set<String> matches = userRefreshTokens.entrySet().stream().filter(entry -> Objects.equals(entry.getValue(),refreshToken)).map(Map.Entry::getKey).collect(Collectors.toSet());
		if (matches != null && matches.size()>0) {
			return matches.iterator().next();
		}
		return null;

	}
	
}
