package com.foster.joy.resources.auth.endpoint;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.foster.joy.errors.BadRequestException;
import com.foster.joy.errors.ForbiddenRequestException;
import com.foster.joy.errors.InternalServerException;
import com.foster.joy.helpers.ResourceHelper;
import com.foster.joy.resources.auth.dao.AuthDAO;
import com.foster.joy.resources.auth.model.AccessToken;
import com.foster.joy.resources.auth.model.AccessTokenWithRefreshToken;
import com.foster.joy.resources.auth.model.LoginRequestParams;
import com.foster.joy.resources.auth.model.RefreshTokenInfo;

@Path("/access-tokens")
public class AuthResource {
	private AuthDAO dao = ResourceHelper.getAuthDAO();

	@POST
	@Path("/refresh")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public AccessToken refreshToken(RefreshTokenInfo refreshToken) {

		try {
			if (refreshToken == null || refreshToken.getRefresh_token() == null
					|| "".equals(refreshToken.getRefresh_token())) {
				throw new BadRequestException("Refresh Token is required");
			}
			return dao.refreshAccessToken(refreshToken.getRefresh_token());
		} catch (BadRequestException | ForbiddenRequestException bre) {
			throw bre;
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response login(LoginRequestParams loginInfo) {
		try {
			if (loginInfo == null) {
				throw new BadRequestException("Email and password are required");
			}
			if (loginInfo.getEmail() == null || "".equals(loginInfo.getEmail())) {
				throw new BadRequestException("Email is required");
			}
			if (loginInfo.getPassword() == null || "".equals(loginInfo.getPassword())) {
				throw new BadRequestException("Password is required");
			}
			AccessTokenWithRefreshToken tok = dao.getNewAccessToken(loginInfo);
			return Response.status(Response.Status.CREATED).entity(tok).build();
		} catch (BadRequestException | ForbiddenRequestException bre) {
			throw bre;
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}

	@DELETE
	@Produces({ MediaType.APPLICATION_JSON })
	public Response logout(@HeaderParam("X-Access-Token") String token, RefreshTokenInfo refreshToken) {
		try {
			AuthResource.checkAccessToken(token);
			if (refreshToken == null || refreshToken.getRefresh_token() == null
					|| "".equals(refreshToken.getRefresh_token())) {
				throw new BadRequestException("Refresh Token is required");
			}
			String email = ResourceHelper.getEmailFromToken(token);
			// instructions weren't clear if logout kills accessToken or Refresh Token or
			// both, so expiring both
			dao.deleteAccessToken(token, email);
			dao.deleteRefreshToken(token, email);
			return Response.status(Response.Status.NO_CONTENT).build();
		} catch (BadRequestException | ForbiddenRequestException bre) {
			throw bre;
		} catch (Exception e) {
			throw new InternalServerException(e);
		}
	}

	public static void checkAccessToken(String token) {
		
		if (token == null ) {
			throw new BadRequestException("Access Token is required");
		}
		String email = ResourceHelper.getEmailFromToken(token);
		if (!ResourceHelper.getAuthDAO().isAccessTokenValid(token, email)) {
			throw new BadRequestException("Access Token is invalid");
		}
	}

}
